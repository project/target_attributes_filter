<?php

namespace Drupal\target_attributes_filter\Plugin\Filter;

use Drupal\Component\Utility\Html;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\filter\FilterProcessResult;
use Drupal\filter\Plugin\FilterBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Provides a filter to manage attributes on external URLs.
 *
 * @Filter(
 *   id = "filter_target_attributes",
 *   title = @Translation("Add target attribute to links"),
 *   type = Drupal\filter\Plugin\FilterInterface::TYPE_TRANSFORM_IRREVERSIBLE,
 *   settings = {
 *     "filter_target_attribute" = "_self",
 *     "filter_target_method" = "all",
 *   }
 * )
 */
class TargetAttributesFilter extends FilterBase implements ContainerFactoryPluginInterface {

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected RequestStack $requestStack;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, RequestStack $requestStack) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->requestStack = $requestStack;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('request_stack')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function setConfiguration(array $configuration) {
    parent::setConfiguration($configuration);
    $this->settings += [
      'filter_target_attribute' => '_blank',
      'filter_target_method' => 'all',
      'filter_target_replace' => 1,
    ];
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state): array {
    $form['filter_target_attribute'] = [
      '#type' => 'select',
      '#title' => $this->t('Target attribute'),
      '#options' => [
        '_blank' => $this->t('Open links in a new window or tab'),
        '_self' => $this->t('Opens links in the same frame as it was clicked'),
        '_parent' => $this->t('Opens links in the parent frame'),
        '_top' => $this->t('Opens links in the full body of the window'),
      ],
      '#default_value' => $this->settings['filter_target_attribute'],
      '#required' => TRUE,
    ];
    $form['filter_target_method'] = [
      '#type' => 'radios',
      '#title' => $this->t('Links'),
      '#options' => [
        'all' => $this->t('All links'),
        'internal' => $this->t('Only internal links'),
        'external' => $this->t('Only external links'),
      ],
      '#default_value' => $this->settings['filter_target_method'],
      '#required' => TRUE,
    ];
    $form['filter_target_replace'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Replace target attribute'),
      '#description' => $this->t('If checked, any existing target attributes value is replaced.'),
      '#default_value' => $this->settings['filter_target_replace'],
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function process($text, $langcode): FilterProcessResult {
    $dom = Html::load($text);
    $internalHost = $this->requestStack->getMainRequest()->getHttpHost();
    foreach ($dom->getElementsByTagName('a') as $link) {
      /** @var \DOMElement $link */
      if (!$this->settings['filter_target_replace'] && $link->hasAttribute('target')) {
        continue;
      }
      if ($href = trim($link->getAttribute('href'))) {
        $url = parse_url($href);
        $host = preg_replace('/^www\./i', '', $url['host'] ?? $internalHost);
        if ($this->settings['filter_target_method'] === 'internal' && $host !== $internalHost) {
          continue;
        }
        elseif ($this->settings['filter_target_method'] === 'external' && $host === $internalHost) {
          continue;
        }
        $link->setAttribute('target', $this->settings['filter_target_attribute']);
      }
    }
    return new FilterProcessResult(Html::serialize($dom));
  }

}
