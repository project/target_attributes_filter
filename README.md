# Target Attributes Filter

Override the hyperlink target attribute using text formats. Choose the target attribute value and whether it applies
to all links, only internal, or only external links.

## Usage

1. Download and install the `drupal/target_attributes_filter` module. Recommended install method is composer:
   ```
   composer require drupal/target_attributes_filter
   ```
2. Go to the "Text formats and editors" configuration page (/admin/config/content/formats).
3. Edit the desired text format.
4. Enable the "Add target attribute to links" filter.
5. Review available configurations under filter settings and save changes.
